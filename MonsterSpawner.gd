extends Area2D

	
export(Array, Dictionary) var _waves = [
	{
		"zombies": 30,
		"werewolves": 0,
		"vampires": 0,
		"total_time": 30.0
	},
	{
		"zombies": 40,
		"werewolves": 1,
		"vampires": 0,
		"total_time": 30.0
	},
	{
		"zombies": 40,
		"werewolves": 1,
		"vampires": 1,
		"total_time": 30.0
	},
	{
		"zombies": 50,
		"werewolves": 2,
		"vampires": 1,
		"total_time": 30.0
	},
	{
		"zombies": 100,
		"werewolves": 5,
		"vampires": 3,
		"total_time": 30.0
	},
]

var zombie_count : int
var werewolf_count : int
var vampire_count : int

func _ready() -> void:
	$ZombieTimer.connect("timeout", self, "spawn_zombie")
	$WerewolfTimer.connect("timeout", self, "spawn_werewolf")
	$VampireTimer.connect("timeout", self, "spawn_vampire")
	
	for wave in _waves:
		yield(get_tree().create_timer(10.0), "timeout")
		$WaveTimer.wait_time = wave.total_time + 10
		$ZombieTimer.wait_time = wave.total_time / (wave.zombies + 1)
		$WerewolfTimer.wait_time = wave.total_time / (wave.werewolves + 1)
		$VampireTimer.wait_time = wave.total_time / (wave.vampires + 1)
		
		for timer in [$WaveTimer, $ZombieTimer, $WerewolfTimer, $VampireTimer]:
			timer.start()
		
		zombie_count = wave.zombies
		werewolf_count = wave.werewolves
		vampire_count = wave.vampires
		
		yield($WaveTimer, "timeout")

func spawn_zombie() -> void:
	if zombie_count > 0:
		var new_zombie = preload("res://Zombie.tscn").instance()
		add_child(new_zombie)
		new_zombie.global_position.x = position.x
		new_zombie.global_position.y = rand_range(40, 1040)
		
		zombie_count -= 1
		
func spawn_werewolf() -> void:
	if werewolf_count > 0:
		var new_werewolf = preload("res://Werewolf.tscn").instance()
		add_child(new_werewolf)
		new_werewolf.global_position.x = position.x
		new_werewolf.global_position.y = rand_range(0, 1040)
		
		werewolf_count -= 1
		
func spawn_vampire() -> void:
	if vampire_count > 0:
		var new_vampire = preload("res://Vampire.tscn").instance()
		add_child(new_vampire)
		new_vampire.global_position.x = position.x
		new_vampire.global_position.y = rand_range(0, 1040)
		
		vampire_count -= 1
