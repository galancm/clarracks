extends Sprite


func _ready() -> void:
	set_process_input(false)
	
	yield(get_tree().create_timer(2), "timeout")
	set_process_input(true)


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		get_tree().change_scene("res://TowerField.tscn")
