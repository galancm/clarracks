extends Sprite

var units = []

var has_shouted = false

func _ready() -> void:
	for child in get_children():
		if not child is Pursuer:
			continue
		var child_position = child.global_position
		remove_child(child)
		child.global_position = child_position
		units.append(child)
		
		child.spawner = self 
	
	$Sprite.z_index = $Sprite.global_position.y + 100
		
	yield($AnimationPlayer, "animation_finished")
	for child in units:
		get_tree().get_nodes_in_group("TowerField")[0].add_child(child)

func _process(delta: float) -> void:
	var surviving_units := false
	for unit in units:
		if is_instance_valid(unit):
			surviving_units = true
			break
	
	if not surviving_units:
		queue_free()
