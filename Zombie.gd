extends Pursuer
class_name Zombie


var push_vector : Vector2


func _ready() -> void:
	$Groan.volume_db = rand_range(-30, -10)
	$Groan/Timer.wait_time = 1.0 + rand_range(0, 3.0)
	$Groan/Timer.start()
	$Groan/Timer.connect("timeout", $Groan, "play")

	yield($Groan/Timer, "timeout")
	$Groan/Timer.wait_time = 5.0 + rand_range(0, 15.0)
	$Groan/Timer.start()

func _physics_process(delta: float) -> void:
	_find_target("Clerics")

	if _target and is_instance_valid(_target):
		_pursue()
	elif global_position.x < 320:
		_attack_clarracks()
	else:
		_move_to_goal(Vector2(0, global_position.y), delta)
		
	move_and_slide(push_vector * 200)
	push_vector = Vector2(0,0)

func push(vector: Vector2):
	push_vector = vector
	
func _attack_clarracks():
	var clarracks = Helpers.get_unique_node("Clarracks")

	if not ($Sprite.playing and $Sprite.animation == "attack"):
		move_and_slide(global_position.direction_to(clarracks.global_position) * speed)
	
		var colliding_with_clarracks = false
		for collider_id in get_slide_count():
			var collision := get_slide_collision(collider_id)
			if collision.collider == clarracks:
				colliding_with_clarracks = true
				break
		if colliding_with_clarracks:
			$Sprite.play("attack")
	
			yield($Sprite, "animation_finished")
			clarracks.health -= damage
			$Sprite.frame = 0
			$Sprite.stop()
		else:
			$Sprite.play("walk")


func queue_free():
	.queue_free()
	
	var saved = preload("res://Saved.tscn").instance()
	get_tree().get_nodes_in_group("TowerField")[0].add_child(saved)
	saved.global_position = global_position
	saved.flip_h = $Sprite.flip_h
