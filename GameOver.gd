extends CenterContainer

func _ready() -> void:
	get_tree().paused = true
	
	if not OS.has_feature('JavaScript'):
		$HBoxContainer/Button.text = "Quit"


func _on_Button_pressed() -> void:
	if OS.has_feature('JavaScript'):
		JavaScript.eval("location.reload()")
	else:
		get_tree().quit
