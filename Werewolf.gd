extends Pursuer


func _physics_process(delta: float) -> void:
	var old_target = _target
	_find_target("Clerics")
	
	if old_target != _target and $GrowlPlayer.playing == false:
		$GrowlPlayer.play()

	if _target and is_instance_valid(_target):
		_pursue()
	else:
		_move_to_goal(Vector2(0, global_position.y), delta)
		
	for collision_number in range(0, get_slide_count()):
		var collision := get_slide_collision(collision_number)
		
		if collision.collider is Zombie:
			collision.collider.push(global_position.direction_to(collision.position))  
