extends Pursuer


onready var starting_position := global_position

var spawner = null

func _physics_process(delta: float) -> void:
	_find_target("Enemies")

	if _target and is_instance_valid(_target):
		_pursue()
		if spawner and spawner.has_shouted == false:
			$ShoutPlayer.play()
			spawner.has_shouted = true
	else:
		_move_to_goal(starting_position, delta)

	if health <= 0:
		queue_free()
