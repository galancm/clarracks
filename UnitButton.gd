extends Button

export var unit_type: String

signal unit_selected(type)

func _pressed() -> void:
	emit_signal("unit_selected", unit_type)
