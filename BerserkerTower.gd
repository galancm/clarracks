extends Area2D

var current_spawner = null

func _input_event(viewport: Object, event: InputEvent, shape_idx: int) -> void:
	if not current_spawner:
		var mouse_event = event as InputEventMouseButton
		if mouse_event and mouse_event.button_index == BUTTON_LEFT:
			var unit_selector = Helpers.get_unique_node("UnitSelector")
			var spawn_type = unit_selector.current_unit
			if spawn_type != null:
				var spawner = load("res://" + spawn_type.capitalize() + "Spawn.tscn").instance()
				
				var do_spawn = false
				match spawn_type:
					"templar":
						if unit_selector.population - unit_selector.templar_cost >= 0:
							unit_selector.population -= unit_selector.templar_cost
							do_spawn = true
					"berserker":
						if unit_selector.population - unit_selector.berserker_cost >= 0:
							unit_selector.population -= unit_selector.berserker_cost
							do_spawn = true
					"medic":
						if unit_selector.population - unit_selector.medic_cost >= 0:
							unit_selector.population -= unit_selector.medic_cost
							do_spawn = true
				
				if do_spawn == true:
					spawner.global_position = global_position
					get_tree().get_nodes_in_group("TowerField")[0].add_child(spawner)
					current_spawner = spawner
					
					yield(spawner, "tree_exited")
					current_spawner = null
