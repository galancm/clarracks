extends Panel

var current_unit = null
export var population : int = 20 setget _set_population

export var templar_cost := 3
export var berserker_cost := 10
export var medic_cost := 5

func _ready() -> void:
	Helpers.set_unique_node("UnitSelector", self)
	_set_population(population)

	for unit_button in $Buttons.get_children():
		unit_button.connect("unit_selected", self, "_select_unit")

func _select_unit(type):
	current_unit = type
	
	for unit_button in $Buttons.get_children():
		if unit_button is Button and unit_button.unit_type != type:
			unit_button.pressed = false

func _set_population(value):
	population = value
	$Buttons/PopulationLabel.text = "Population: " + str(population)
