extends StaticBody2D

export var max_health := 5000.0

onready var health := max_health setget _set_health

func _ready() -> void:
	Helpers.set_unique_node("Clarracks", self)
	
	z_index = global_position.y

func _set_health(value):
	health = value
	if $HealthBar:
		$HealthBar.value = health / max_health
	
	if health <= 0:
		get_tree().get_nodes_in_group("TowerField")[0].add_child( preload("res://GameOver.tscn").instance() )
