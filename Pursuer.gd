extends KinematicBody2D
class_name Pursuer

export var speed := 200.0
export var aggro_range : float
export var attack_range := 50.0
export var max_health := 100.0
export var damage := 10.0
export var heals := false

onready var health := max_health setget _set_health

var _target = null


func _find_target(target_group: String):
	if !heals:
		var target_distance := 3840.0
		for target in get_tree().get_nodes_in_group(target_group):
			var distance_to_target := global_position.distance_to(target.global_position)
			if distance_to_target < aggro_range and distance_to_target < target_distance:
				_target = target
				target_distance = distance_to_target
	else:
		var target_weight := 0.0
		for target in get_tree().get_nodes_in_group(target_group):
			if target == self:
				continue
			var distance_to_target := global_position.distance_to(target.global_position)
			if distance_to_target < aggro_range:
				var new_weight = ( (aggro_range - distance_to_target) / aggro_range ) * (float(target.max_health - target.health) / target.max_health)
				if new_weight > target_weight:
					_target = target
func _pursue():
	z_index = global_position.y + 100
	if global_position.distance_to(_target.global_position) < attack_range:
		if not ($Sprite.is_playing() and $Sprite.animation == "attack"):
			$Sprite.play("attack")
			
			if self.get("is_berserker") == true and _target.get("is_vampire") == true:
				_target.stun()

			yield($Sprite, "animation_finished")
			if is_instance_valid(_target):
				if !heals:
					_target.health -= damage
					if get_node_or_null("ClankPlayer") != null:
						$ClankPlayer.play()
				else:
					_target.health += damage
				$Sprite.frame = 0
				$Sprite.stop()
	else:
		$Sprite.play("walk")
		var movement = global_position.direction_to(_target.global_position) * speed
		if movement.x < 0 and global_position.distance_to(_target.global_position) > 5.0:
			$Sprite.flip_h = true
		else:
			$Sprite.flip_h = false
		move_and_slide( movement )

func _move_to_goal(goal: Vector2, delta):
	z_index = global_position.y + 100
	$Sprite.play("walk")
	_target = null
	var navigation := Helpers.get_unique_node("Navigation") as Navigation2D
	if navigation == null:
		return
	var path := navigation.get_simple_path(global_position, goal)
	if global_position.distance_to(path[1]) > speed * delta:
		var movement = global_position.direction_to(path[1]) * speed
		if movement.x < 0 and global_position.distance_to(path[1]) > 2.0:
			$Sprite.flip_h = true
		else:
			$Sprite.flip_h = false
		move_and_slide( movement )
	else:
		$Sprite.flip_h = false
		$Sprite.stop()


func _set_health(value):
	health = value
	if health > max_health:
		health = max_health
	elif health <= 0:
		if is_in_group("Enemies"):
			Helpers.get_unique_node("UnitSelector").population += 1
		queue_free()
	$AnimationPlayer.play("TakeDamage")
