extends Pursuer


const is_vampire = true

export var faded := false

var stunned := false

func _ready() -> void:
	$AttackTimer.connect("timeout", self, "_aoe")
	$StunTimer.connect("timeout", self, "_unstun")


func _physics_process(delta: float) -> void:
	_find_target("Clerics")

	if not stunned:
		if _target and is_instance_valid(_target):
			_pursue()
		elif global_position.x < 320:
			_attack_clarracks()
		else:
			_move_to_goal(Vector2(0, global_position.y), delta)
		
	var fade := false
	for body in $Area2D.get_overlapping_bodies():
		if body is Pursuer:
			if body is Berserker:
				fade = false
				break
			else:
				fade = true
				
	if fade:
		$Sprite.play("mist")
		$Sprite.self_modulate = Color(1,1,1,0.7)
	else:
		$Sprite.self_modulate = Color(1,1,1,1)


func _aoe():
	for cleric in get_tree().get_nodes_in_group("Clerics"):
		if is_instance_valid(cleric) and global_position.distance_to(cleric.global_position) < attack_range:
			
			if not $CPUParticles2D.emitting and not $CPUParticles2D2.emitting:
				$BatPlayer.play("Bat")
				
				yield($BatPlayer, "animation_finished")
				if is_instance_valid(cleric):
					cleric.health -= damage


func stun():
	stunned = true
	$StunTimer.start()


func _unstun():
	stunned = false


func _attack_clarracks():
	var clarracks = Helpers.get_unique_node("Clarracks")
	var clarracks_position = clarracks.global_position + Vector2(0, -125) + Vector2(rand_range(-50,50), rand_range(-50,50))

	if not ($Sprite.playing and $Sprite.animation == "attack"):
		if global_position.distance_to(clarracks_position) < 50:
			if not $BatPlayer.is_playing():
				$BatPlayer.play("Bat")
				
				yield($BatPlayer, "animation_finished")
				clarracks.health -= damage
		else:
			move_and_slide(global_position.direction_to(clarracks_position) * speed)
			$Sprite.play("walk")
