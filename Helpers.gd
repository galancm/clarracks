extends Node

var _unique_nodes := {}

func set_unique_node(name: String, node: Node) -> bool:
	if not name in _unique_nodes:
		_unique_nodes[name] = node
		return true
	else:
		return false

func get_unique_node(name: String) -> Node:
	return _unique_nodes[name]
