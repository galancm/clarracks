extends Pursuer
class_name Berserker


const is_berserker = true

onready var _initial_y := global_position.y

var spawner = null


func _physics_process(delta: float) -> void:
	_find_target("Enemies")

	if _target and is_instance_valid(_target):
		_pursue()
		if spawner and spawner.has_shouted == false:
			$ShoutPlayer.play()
			spawner.has_shouted = true
	else:
		var goal = Vector2(1950, _initial_y)
		_move_to_goal(goal, delta)
		if global_position.x > 1920:
			queue_free()

	if health <= 0:
		queue_free()
